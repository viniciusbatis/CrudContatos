import Vue from 'vue';
import Router from 'vue-router';

import MostrarContatos from '@/components/visualizacao/MostrarContatos';
import CriarContatos from '@/components/criacao/CriarContatos';
import MaximizarContato from '@/components/visualizacao/MaximizarContato';
import ListarContatos from '@/components/listagem/ListarContatos';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MostrarContatos',
      component: MostrarContatos,
    },
    {
      path: '/criar-contatos',
      name: 'CriarContatos',
      component: CriarContatos,
    },
    {
      path: '/editar-contatos/:id',
      name: 'EditarContatos',
      component: CriarContatos,
    },
    {
      path: '/maximizar-contato/:id',
      name: 'MaximizarContato',
      component: MaximizarContato,
    },
    {
      path: '/listar-contatos',
      name: 'ListarContatos',
      component: ListarContatos,
    },
  ],
});
