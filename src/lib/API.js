import ezFetch from 'ez-fetch';

const LINK_API = 'https://5b1ac4d083b6190014ca3b06.mockapi.io/contatos';

export default {
  pegarContatos() {
    return ezFetch.get(LINK_API);
  },
  pegarContato(id) {
    return ezFetch.get(`${LINK_API}/${id}`);
  },
  criarContato(contatos) {
    return ezFetch.post(LINK_API, contatos);
  },
  deletarContato(id) {
    return ezFetch.delete(`${LINK_API}/${id}`);
  },
  atualizarContato(id, contato) {
    return ezFetch.put(`${LINK_API}/${id}`, contato);
  },
};
